import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MatAutocompleteModule,
  MatButtonModule, MatCardModule, MatDialogModule, MatFormFieldModule, MatGridListModule,
  MatIconModule, MatInputModule, MatListModule, MatNativeDateModule, MatSelectModule,
  MatSidenavModule, MatTabsModule, MatToolbarModule, MatStepperModule, MatSnackBarModule, MatRadioModule,
  MatCheckboxModule, MatExpansionModule
} from '@angular/material';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { HomeComponent } from './home/home/home.component';
import { FormularioComponent } from './pages/formulario/formulario.component';
import { TabsComponent } from './pages/tabs/tabs.component';
import { LoginComponent } from './pages/login/login.component';
import { PagesComponent } from './pages/pages.component';
import { TemplateComponent } from './pages/template/template.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PagesRoutingModule } from '../app/pages/pages-routing.module';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {AgmCoreModule} from '@agm/core';
import { IntroloaderComponent } from './pages/introloader/introloader.component';
import { VerifyphoneAndCodeComponent } from './pages/login/components//verifyphone-and-code/verifyphone-and-code.component';
import { PersonalInformationComponent } from './pages/login/components/personal-information/personal-information.component';
import { LoginFormComponent } from './pages/login-form/login-form.component';
import { GoogleMapSearchComponent } from './pages/google-map-search/google-map-search.component';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { FavoritesModalComponent } from './pages/favorites-modal/favorites-modal.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FormularioComponent,
    TabsComponent,
    LoginComponent,
    VerifyphoneAndCodeComponent,
    PersonalInformationComponent,
    PagesComponent,
    TemplateComponent,
    DashboardComponent,
    IntroloaderComponent,
    LoginFormComponent,
    GoogleMapSearchComponent,
    FavoritesModalComponent
  ],
  imports: [
    PagesRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NgbModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDJSA1xCSFDonR5NuKjWcgpGw-GDzJQq4I'
    }),
    MatStepperModule,
    MatToolbarModule,
    MatIconModule,
    MatSidenavModule,
    MatListModule,
    MatTabsModule,
    MatButtonModule,
    MatDialogModule,
    MatInputModule,
    MatGridListModule,
    MatCardModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSnackBarModule,
    MatRadioModule,
    MatIconModule,
    MatCheckboxModule,
    MatExpansionModule
  ],
  entryComponents: [
    FavoritesModalComponent
  ],
  providers: [
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
