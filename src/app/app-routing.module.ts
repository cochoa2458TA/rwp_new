import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {FormularioComponent} from './pages/formulario/formulario.component';
import {TabsComponent} from './pages/tabs/tabs.component';
import {LoginComponent} from './pages/login/login.component';
import {TemplateComponent} from './pages/template/template.component';
import {IntroloaderComponent} from './pages/introloader/introloader.component'
import {LoginFormComponent} from './pages/login-form/login-form.component'

const routes: Routes = [
  { path: '',
    redirectTo: 'introloader',
    pathMatch: 'full',
  },
  { path: 'login', component: LoginFormComponent },
  { path: 'registration', component: LoginComponent },
  { path: 'introloader', component: IntroloaderComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(
      routes,
      { enableTracing: true }
  )],
  exports: [RouterModule]
})
export class AppRoutingModule { }
