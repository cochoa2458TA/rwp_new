import { Component, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material';

@Component({
  selector: 'app-favorites-modal',
  templateUrl: './favorites-modal.component.html',
  styleUrls: ['./favorites-modal.component.scss']
})
export class FavoritesModalComponent implements OnInit {
  folders = [
    {
      name: 'Dr. Amer',
      address: '1245 Northwest 2nd Avenue, Miami, FL, USA',
    },
    {
      name: 'Chiropractor',
      address: '1245 Northwest 2nd Avenue, Miami, FL, USA',
    },
    {
      name: 'Home',
      address: '1245 Northwest 2nd Avenue, Miami, FL, USA',
    }
  ];

  constructor() { }

  ngOnInit() {
  }
  step = 0;

  setStep(index: number) {
    this.step = index;
  }

  nextStep() {
    this.step++;
  }

  prevStep() {
    this.step--;
  }
}
