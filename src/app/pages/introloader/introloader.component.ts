import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';


@Component({
  selector: 'app-introloader',
  templateUrl: './introloader.component.html',
  styleUrls: ['./introloader.component.scss']
})
export class IntroloaderComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {

    const $wheel = document.getElementById('wheelId')
    const $letter = document.getElementById('letterId')
    const $containerLoading = document.getElementById('container-loading-id')
    
    $wheel.addEventListener('animationend',(event)=>{
      $letter.className += " animate-letter"
      $wheel.className += " animate-wheel"
  
    })
    $letter.addEventListener('animationend',(event)=>{
      $wheel.setAttribute("style", "top: 0px !important;")
      $containerLoading.className += " fade-out-loading"
      setTimeout(()=>{
        console.log('setInterval');
        this.router.navigate(['/registration']);
      },3000);
    });
  }

}
