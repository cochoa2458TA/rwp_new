import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntroloaderComponent } from './introloader.component';

describe('IntroloaderComponent', () => {
  let component: IntroloaderComponent;
  let fixture: ComponentFixture<IntroloaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntroloaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntroloaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
