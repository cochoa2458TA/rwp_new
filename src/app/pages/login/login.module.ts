import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginRoutingModule } from './login-routing.module';
import { VerifyphoneAndCodeComponent } from './components/verifyphone-and-code/verifyphone-and-code.component';
import { VerifyphoneAndCodeModule } from './components/verifyphone-and-code/verifyphone-and-code.module';
import { PersonalInformationComponent } from './components/personal-information/personal-information.component';

@NgModule({
  imports: [
    CommonModule,
    LoginRoutingModule,
    VerifyphoneAndCodeModule
  ],
  declarations: [VerifyphoneAndCodeComponent, PersonalInformationComponent]
})
export class LoginModule { }
