import { Component, OnInit,Output,EventEmitter,Input } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-verifyphone-and-code',
  templateUrl: './verifyphone-and-code.component.html',
  styleUrls: ['./verifyphone-and-code.component.scss']
})
export class VerifyphoneAndCodeComponent implements OnInit {
  phoneCodeFormGroup: FormGroup;

  @Input() verificationCodeSuccessInput:boolean=false;
  @Output() verifyPhoneEmit : EventEmitter<any> = new EventEmitter<any>()
  constructor(private _formBuilder: FormBuilder) { }

  ngOnInit() {
    this.phoneCodeFormGroup = this._formBuilder.group({
      phoneCtrl: ['', Validators.required],
      verifCodeCtrl:['',Validators.required]
    });
  }

  public onKeyUpVerifyPhone(event){
    console.log(this.phoneCodeFormGroup.get('phoneCtrl').value)
    if(this.phoneCodeFormGroup.get('phoneCtrl').value!=""){
      this.verifyPhoneEmit.emit(this.phoneCodeFormGroup)
    }else{
      this.verifyPhoneEmit.emit(this.phoneCodeFormGroup);
    }
  }
}
