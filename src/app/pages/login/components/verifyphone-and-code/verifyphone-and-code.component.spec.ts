import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifyphoneAndCodeComponent } from './verifyphone-and-code.component';

describe('VerifyphoneAndCodeComponent', () => {
  let component: VerifyphoneAndCodeComponent;
  let fixture: ComponentFixture<VerifyphoneAndCodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerifyphoneAndCodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerifyphoneAndCodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
