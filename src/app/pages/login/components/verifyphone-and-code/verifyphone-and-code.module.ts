import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VerifyphoneAndCodeComponent } from './verifyphone-and-code.component';

@NgModule({
  imports: [
    CommonModule
  ],
  exports:[
    VerifyphoneAndCodeComponent
  ],
  declarations: [
    VerifyphoneAndCodeComponent
  ]
})
export class VerifyphoneAndCodeModule { }
