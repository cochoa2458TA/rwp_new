import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-personal-information',
  templateUrl: './personal-information.component.html',
  styleUrls: ['./personal-information.component.scss']
})
export class PersonalInformationComponent implements OnInit {
  personalInfoFormGroup: FormGroup;
  public genders = [
    {value: 'M'  , description : 'Masculine'},
    {value: 'F'  , description : 'Femenine'},
  ];
  constructor(private _formBuilder: FormBuilder) { }

  ngOnInit() {
    this.personalInfoFormGroup = this._formBuilder.group({
      email: ['', Validators.required],
      gender:['',Validators.required],
      firstName:['',Validators.required],
      middleInitial:[''],
      lastName:['',Validators.required],
      user:['',Validators.required],
      password:['',Validators.required],
    });
  }

}
