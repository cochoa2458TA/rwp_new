import {Router} from '@angular/router';
import { Component, OnInit,ElementRef,ViewChild } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  @ViewChild('verificationcodeInput') verificationcodeInput:any
  public phoneCodeData            : any;
  public isSendPhoneNumber        : boolean = false;
  public isSendVerifCode          : boolean = false;
  public verificationCodeSuccess  : boolean = false;
  public isNextBtn                : boolean = false;

  isLinear = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  

  constructor(private router: Router,private _formBuilder: FormBuilder,public snackBar: MatSnackBar) { }

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
  }
  /**
   * onClickSubmitPersonalInfo this method sends the info of the rider 
   */
  public onClickSubmitPersonalInfo() {
    this.router.navigate(['/login']);
  }
  /**
   * onVerifyEmit this method is used to appear the button to send the phone number
   */
  public onVerifyEmit(event) {
    console.log(event)
    this.phoneCodeData = event;
    this.isSendPhoneNumber = event.controls.phoneCtrl.value!=""?true:false
  }
  /**
   * onClickSendPhone this method send the phone through the service and retrieve a code to verify
   */
  public onClickSendPhone() {    
    // "start" | "center" | "end" | "left" | "right"
    this.snackBar.open('The code has been send the number '+this.phoneCodeData.controls.phoneCtrl.value,"", {
      duration:2000,
      horizontalPosition: "center",
      verticalPosition:"top"
    })
    this.verificationCodeSuccess= true
    this.isSendPhoneNumber = false;
    this.isSendVerifCode = true
  }
  /**
   * onClickSendVerifCode send the code to verify it
   */
  public onClickSendVerifCode() {
    this.isSendVerifCode = false;
    this.isNextBtn = true
  }
  onClickLogin() {
    this.router.navigate(['/dashboard']);
  }
}
