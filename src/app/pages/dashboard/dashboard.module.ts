import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {GoogleMapSearchModule} from '../google-map-search/google-map-search.module';

@NgModule({
  imports: [
    CommonModule,
    GoogleMapSearchModule
  ],
  declarations: []
})
export class DashboardModule { }
