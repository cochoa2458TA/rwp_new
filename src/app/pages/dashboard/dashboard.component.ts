import {AfterViewInit, Component, OnChanges, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatDialog} from '@angular/material';
import {FavoritesModalComponent} from '../favorites-modal/favorites-modal.component'
import {  } from '@types/googlemaps';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnChanges{
  public arrayOfTimes       : any = [];
  public locationPickUp     : any;
  public locationDropOff    : any;
  public isPickUpAndDest    : boolean;
  public isEstimatedSection : boolean = false;
  public isRoundTrip        : boolean = false;
  public isCenterMarker     : boolean = false;
  public isEstimateSection  : boolean = false;
  dashboardFormGroup        : FormGroup;
  map                       : google.maps.Map;
  @ViewChild('gmap') gmapElement: any;
  services = [
    {value: '1'  , description : 'Ambulatory'},
    {value: '2'  , description : 'Wheelchair'},
    {value: '3'  , description : 'Stretcher'}
  ];
  reasons = [
    {value: '1'  , description : 'Going to the doctor'},
    {value: '2'  , description : 'Going Home'}
  ];
  title:  'My first AGM project';
  lat:  51.678418;
  lng:  7.809007;
  constructor(private _dashboardFormBuilder: FormBuilder,public dialog: MatDialog) {}
  ngOnInit(){
    this.dashboardReactiveForm()
    this.timesArray()
  }

  ngOnChanges(){

  }
  onChangWillCall(event){
    event.checked?this.dashboardFormGroup.get('returntime').disable():this.dashboardFormGroup.get('returntime').enable();
  }

  /**
   * this method will open the favorites modal from the google autocomplete component
   */
  onOpenFavoritesEmit(){
    const favModal = this.dialog.open(FavoritesModalComponent,{
      width:'450px'
    });
    favModal.afterClosed().subscribe(
        result =>{console.log('Dialog result:',result)}
    )
  }
  /**
   * this method validates if the service section is valid
   * @returns {boolean}
   */
  validateServiceFields(){
    let calltype    = this.dashboardFormGroup.get('calltypeid').value;
    let reason      = this.dashboardFormGroup.get('tripreasontransportid').value;
    let DOS         = this.dashboardFormGroup.get('dateofservice').value;
    let appmnt      = this.dashboardFormGroup.get('appointment').value;
    if((calltype==""||calltype==null)||(reason==""||reason==null)||(DOS==""||DOS==null)||(appmnt==""||appmnt==null)){
      this.isEstimateSection = false
      return false
    }else{
      return true
    }
  }

  /**
   * this method create the reactive form model
   * @returns {FormGroup}
   */
  dashboardReactiveForm(){
    return this.dashboardFormGroup = this._dashboardFormBuilder.group({
      triplocationpickup    :this._dashboardFormBuilder.group({
        modifierid      : [''],
        latitude        : [''],
        longitude       : [''],
        location        : [''],
        streetaddress   : [''],
        roomaptnumber   : [''],
        city            : [''],
        state           : [''],
        zip             : [''],
        phonenumber     : ['']
      }),
      triplocationdropoff   :this._dashboardFormBuilder.group({
        modifierid      : [''],
        latitude        : [''],
        longitude       : [''],
        location        : [''],
        streetaddress   : [''],
        roomaptnumber   : [''],
        city            : [''],
        state           : [''],
        zip             : [''],
        phonenumber     : ['']
      }),
      tripstatusid          : [''],
      companyid             : [''],
      leg                   : [''],
      riderid               : [''],
      payorid               : [''],
      calltypeid            : ['',Validators.required],
      callreceived          : [''],
      dateofservice         : ['',Validators.required],
      driverid              : [''],
      caller                : [''],
      callerphone           : [''],
      isloadedmiles         : [''],
      appointment           : ['',Validators.required],
      pickup                : [''],
      returntime            : [''],
      hasreturn             : [''],
      estimatedtime         : [''],
      totalmiles            : [''],
      totalamount           : [''],
      notetrip              : [''],
      notepickup            : [''],
      notedropoff           : [''],
      payorauthorization    : [''],
      tripreasontransportid : ['',Validators.required],
    })
  }
  /**
   * this method generate an array of times
   */
  timesArray(){
    var x = 15; //minutes interval
    var times = []; // time array
    var tt = 0; // start time
    var ap = ['AM', 'PM']; // AM-PM

    //loop to increment the time and push results in array
    for (var i=0;tt<24*60; i++) {
      var hh = Math.floor(tt/60); // getting hours of day in 0-24 format
      var mm = (tt%60); // getting minutes of the hour in 0-55 format
      times[i] = ("0" + (hh % 12)).slice(-2) + ':' + ("0" + mm).slice(-2) + ap[Math.floor(hh/12)]; // pushing data in array in [00:00 - 12:00 AM/PM format]
      tt = tt + x;
    }
    this.arrayOfTimes = times;
  }
  ngAfterViewInit() {
    const mapProp = {
      center: new google.maps.LatLng(26.281942, -80.146189),
      zoom: 10,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      zoomControl: true,
      scaleControl: true
    };

    this.map = new google.maps.Map(this.gmapElement.nativeElement, mapProp);
  }

  /**
   * this event fires after the pickup dropoff component is valid
   * @param event
   */
  onValidFormPudo(event){
    this.isEstimateSection=event
  }

  /**
   * this method receive the information of google-map-search component of the pickup and dropof fields
   * @param event
   */
  onPudoInfo(event){
    this.dashboardFormGroup.get('triplocationpickup').setValue(event.triplocationpickup)
    this.dashboardFormGroup.get('triplocationdropoff').setValue(event.triplocationdropoff)

  }
  /**
   * this method receive true or false from google-map-search component to show/hide the centered static marker
   * @param event
   */
  onValidPudoFormForMarker(event){
    this.isPickUpAndDest=event
  }
  /**
   * onValidForm validates if the service section is valid
   */
  public onValidForm() {
    // this.isPickUpAndDest=true
  }
  
  /**
   * onPickUpLocation
   */
  public onPickUpLocation(event) {
    this.locationPickUp = event;
    this.enableEstimateSection()
  }
  /**
   * onDropOffLocation
   */
  public onDropOffLocation(event) {
    this.locationDropOff = event;
    this.enableEstimateSection();
  }
  /**
   * enableEstimateSection
   */
  public enableEstimateSection() {
        // this.isEstimatedSection = this.pickupDropoffFormGroup.valid
  }
  /**
   * onSelectRadio
   */
  public onSelectRadio(event) {
    this.dashboardFormGroup.get('returntime').enable();
    event.value == 2?this.isRoundTrip=true:this.isRoundTrip=false
  }
}
