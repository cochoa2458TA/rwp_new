import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import {  } from '@types/googlemaps';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {
  loginFormGroup: FormGroup;

  constructor(private router: Router,private _loginFormBuilder: FormBuilder) { }

  ngOnInit() {
    // gapi.load('auth2', function() { });
    this.loginFormGroup = this._loginFormBuilder.group({
      user:['',Validators.required],
      password:['',Validators.required],
    });
  }
  onLoginForm(){
    const $animateCard = document.getElementById('loginFormId');
    const $animateLogo = document.getElementById('headerLogo');
    if(!this.loginFormGroup.valid){
      $animateCard.classList.add('animate-card');
      $animateCard.addEventListener('animationend',(event)=>{
        $animateCard.classList.remove('animate-card');
      })
      $animateLogo.classList.add('animate-header-logo');
      $animateLogo.addEventListener('animationend',(event)=>{
        $animateLogo.classList.remove('animate-header-logo');
      })      
    }else{
      this.router.navigate(['/dashboard']);
    }
  }
}
