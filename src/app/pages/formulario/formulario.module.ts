import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormularioRoutingModule } from './formulario-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormularioRoutingModule
  ],
  declarations: []
})
export class FormularioModule { }
