import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TabsComponent} from './tabs/tabs.component';
import {TemplateComponent} from './template/template.component';
import {DashboardComponent} from './dashboard/dashboard.component';

const routes: Routes = [
    {
        path: '',
        component: TemplateComponent,
        children: [
            { path: 'dashboard', component: DashboardComponent },
            { path: 'tabs', component: TabsComponent },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PagesRoutingModule { }
