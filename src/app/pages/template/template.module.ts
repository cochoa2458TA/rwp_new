import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// import { TemplateRoutingModule } from './template-routing.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    // TemplateRoutingModule,
    NgbModule.forRoot()
  ],
  declarations: []
})
export class TemplateModule { }
