import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-google-map-search',
  templateUrl: './google-map-search.component.html',
  styleUrls: ['./google-map-search.component.scss']
})
export class GoogleMapSearchComponent{
  public editBtnPu          : "";
  public editBtnDO          : "";
  public doValue            : boolean = true;
  public puValue            : boolean = true;
  public pudo               : number  = 1;
  public originPlaceId      : any;
  public destinationPlaceId : any;
  public travelMode         : any;
  public directionsService  : any;
  public directionsDisplay  : any;
  public dragEndListener    : any;
  public gSearchFormGroup   : FormGroup;
  public componentLocation = {
    street_number : 'short_name',
    route         : 'short_name',
    locality      : 'short_name',
    administrative_area_level_1: 'short_name',
    country       : 'long_name',
    postal_code   : 'short_name'
  }
  @Input() map:any;
  @Input() mapInput:any;
  @Output() validPudoForm: EventEmitter<any> = new EventEmitter<any>();
  @Output() validPudoFormForMarker: EventEmitter<any> = new EventEmitter<any>();
  @Output() pudoInfo: EventEmitter<any> = new EventEmitter<any>();
  @Output() openFavoritesEmit: EventEmitter<any> = new EventEmitter<any>();

  constructor(private _gSearchFormBuilder: FormBuilder) { }

  ngOnInit() {
    this.gSearchFormGroup = this._gSearchFormBuilder.group({
      pickup:['',Validators.required],
      dropoff:['',Validators.required],
      triplocationpickup:[this._gSearchFormBuilder.group({
        modifierid:17,
        latitude:[''],
        longitude:[''],
        location:[''],
        streetaddress:[''],
        roomaptnumber:[''],
        city:[''],
        state:[''],
        zip:[''],
        phonenumber:['']

      })],
      triplocationdropoff:[this._gSearchFormBuilder.group({
        modifierid:17,
        latitude:[''],
        longitude:[''],
        location:[''],
        streetaddress:[''],
        roomaptnumber:[''],
        city:[''],
        state:[''],
        zip:[''],
        phonenumber:['']
      })]
    })
  }
  gSearchReactiveForm(){

  }

  /**
   * this method will open the favorite dialog throug an @Output()
   */
  onClickFavoritesPU(){
    this.openFavoritesEmit.emit()
  }
  /**
   * this method sends true or false for validate if the fields pickup and dropoff have been filled
   */
  validPudo(){
    if(this.gSearchFormGroup.valid){
      this.validPudoForm.emit(this.gSearchFormGroup.valid)
    }
  }
  onKeyUpPudo(event){
    this.validPudo()
  }
  ngAfterViewInit() {
    let map = this.map
    // Create the search box and link it to the UI element.
    this.autocompleteDirectionsHandler(map);
    this.currentPosition(map);
    // this.directionsDisplay.setMap(null)
  }

  /**
   * this method is called for showPinnedMarker(value)
   * @param map
   */
  addMarkerCenterPinned(map){
    let me = this;
    this.dragEndListener = map.addListener('dragend',()=>{
      let lat = map.getCenter().lat();
      let lng = map.getCenter().lng();
      let pos = {
        lat: lat,
        lng: lng
      };
      me.getAddress(pos)
    })
  }

  /**
   * this method is called for onClickEditPU(event,number) and onClickEditDO(event,number)
   * @param value
   */
  showPinnedMarker(value){

    if(!value){
      this.validPudoFormForMarker.emit(true);
      this.addMarkerCenterPinned(this.map);
    }else{
      this.validPudoFormForMarker.emit(false);
      google.maps.event.clearListeners(this.map, 'dragend');
    }
  }

  /**
   * this method switch the buttons insid the pu/do fields and show or hide the pinnedMarkerCentered
   * @param number
   */
  switchPinnedMarkerBtn(pudo){
    if(pudo=='PU'){
      this.puValue = !this.puValue;
      if(!this.doValue){this.doValue = true}
    }else{
      this.doValue = !this.doValue;
      if(!this.puValue){this.puValue = true;}
    }
  }
  /**
   * this method fires after click the button inside the field pickup location and shows a pinned marker,
   * that returns the position of the center of the map
   * @param event
   * @param number
   */
  onClickEditPU(event,number){
    this.switchPinnedMarkerBtn('PU');
    this.showPinnedMarker(this.puValue);
  }
  onClickEditDO(event,number){
    this.switchPinnedMarkerBtn('DO');
    this.showPinnedMarker(this.doValue);
  }
  /**
   * onFocus this method fires after focus PU or DO
   * @param event
   * @param pudo
   */
  onFocus(event,pudo){
    this.pudo = pudo;
    this.validPudo()
  }
  /**
   * THIS METHOD GETS THE CURRENT POSITION LAT-LNG
   * */
  currentPosition(map){
    let me = this;
    let image = {
      url: "assets/icons/myLocation.png",
      // This marker is 20 pixels wide by 32 pixels high.
      size: new google.maps.Size(32, 32),
      // The origin for this image is (0, 0).
      // origin: new google.maps.Point(0, 0),
      // // The anchor for this image is the base of the flagpole at (0, 32).
      // anchor: new google.maps.Point(0, 32)
    };
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function(position) {
        let pos = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };
        var marker = new google.maps.Marker({
          position: pos,
          icon: image,
          animation: google.maps.Animation.DROP,
          // title:"Hello World!"
        });

        // To add the marker to the map, call setMap();
        marker.setMap(map);
        map.setCenter(pos);
        map.setZoom(16);
        me.getAddress(pos)
      });
    }
  }

  /**
   * THIS METHOD GETS THE ADDRESS
   * @param latLng
   */
  getAddress(latLng){
    let geocoder = new google.maps.Geocoder;
    let me = this;
    geocoder.geocode({'location': latLng}, function(results, status) {

      if(me.pudo==1){
        document.getElementById('orig-input').focus();

        me.gSearchFormGroup.patchValue({
          // pickup:results!=null?results[0].formatted_address:'-'
          pickup:results[0].formatted_address
        })
      }else{
        document.getElementById('dest-input').focus();

        me.gSearchFormGroup.patchValue({
          // dropoff:results!=null?results[0].formatted_address:'-'
          dropoff:results[0].formatted_address
        })
      }
    })
  }

  handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
        'Error: The Geolocation service failed.' :
        'Error: Your browser doesn\'t support geolocation.');
  }

  /**
   * HERE STARTS THE GOOGLE AUTOCOMPLETE
   * */
  autocompleteDirectionsHandler(map) {


    this.map = map;
    this.originPlaceId = null;
    this.destinationPlaceId = null;
    this.travelMode = 'WALKING';
    let originInput = <HTMLInputElement> document.getElementById('orig-input');
    let destinationInput = <HTMLInputElement> document.getElementById('dest-input');
    let modeSelector = <HTMLInputElement> document.getElementById('mode-selector');
    this.directionsService = new google.maps.DirectionsService;
    this.directionsDisplay = new google.maps.DirectionsRenderer;
    this.directionsDisplay.setMap(map);

    // var originAutocomplete = new google.maps.places.Autocomplete(originInput, {placeIdOnly: true});
    // var destinationAutocomplete = new google.maps.places.Autocomplete(destinationInput, {placeIdOnly: true});
    var originAutocomplete = new google.maps.places.Autocomplete(originInput);
    var destinationAutocomplete = new google.maps.places.Autocomplete(destinationInput);

    this.setupClickListener('changemode-walking', 'WALKING');
    this.setupClickListener('changemode-transit', 'TRANSIT');
    this.setupClickListener('changemode-driving', 'DRIVING');

    this.setupPlaceChangedListener(originAutocomplete, 'ORIG');
    this.setupPlaceChangedListener(destinationAutocomplete, 'DEST');

    // this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(originInput);
    // this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(destinationInput);
    // this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(modeSelector);
  }
  setupClickListener(id, mode) {
    var radioButton = document.getElementById(id);
    var me = this;
    radioButton.addEventListener('click', function() {
      me.travelMode = mode;
      me.routing();
    });
  };
  setupPlaceChangedListener(autocomplete, mode) {
    var me = this;
    autocomplete.bindTo('bounds', this.map);
    autocomplete.addListener('place_changed', function() {
      var place = autocomplete.getPlace();
      if (!place.place_id) {
        window.alert("Please select an option from the dropdown list.");
        return;
      }
      if (mode === 'ORIG') {
        me.fillPudo(place,mode)
        me.originPlaceId = place.place_id;
      } else {
        me.fillPudo(place,mode)
        me.destinationPlaceId = place.place_id;
      }
      me.routing();
    });
  };
  routing() {
    if (!this.originPlaceId || !this.destinationPlaceId) {
      return;
    }
    var me = this;

    this.directionsService.route({
      origin: {'placeId': this.originPlaceId},
      destination: {'placeId': this.destinationPlaceId},
      travelMode: this.travelMode
    }, function(response, status) {
      if (status === 'OK') {
        me.directionsDisplay.setDirections(response);
      } else {
        window.alert('Directions request failed due to ' + status);
      }
    });
  };

  /**
   * HERE ENDS THE GOOGLE AUTOCOMPLETE
   * */

  /**
   * this method gets the information to set the triplocationpickup and triplocationdropoff
   * @param mode
   */
  fillPudo(place,mode){
    let formLocationPU = this.gSearchFormGroup.get('triplocationpickup')
    let formLocationDO = this.gSearchFormGroup.get('triplocationdropoff')
    if(mode==='ORIG'){
      this.assignValuesToPudoForm(place,formLocationPU)
    }else{
      this.assignValuesToPudoForm(place,formLocationDO)
    }
  }

  /**
   * this method called for fillpudo(place,mode) and assigns the information of each location field (pu/do) to the
   * reactive form of this component
   * @param place
   * @param formLocation
   */
  assignValuesToPudoForm(place,formLocation){
    let objLocation = [{
      modifierid:17,
      latitude:place.geometry.location.lat(),
      longitude:place.geometry.location.lng(),
      location:place.formatted_address,
      streetaddress:place.formatted_address,
      roomaptnumber:"",
      city:"",
      state:"",
      zip:"",
      phonenumber:"",
    }];
    for(let address of place.address_components){
      let addressType=address.types[0];
      if(this.componentLocation[addressType]){
        if (addressType=="street_number"){
        }
        if (addressType==="route"){
        }
        if (addressType==="locality"){
          objLocation[0].city = address.short_name;
        }
        if (addressType==="administrative_area_level_1"){
          objLocation[0].state = address.short_name;
        }
        if (addressType==="country"){
        }
        if (addressType==="postal_code"){
          objLocation[0].zip = address.short_name;
        }
      }
    }
    formLocation.setValue(objLocation[0])
    if(this.gSearchFormGroup.valid){
      this.pudoInfo.emit(this.gSearchFormGroup.value)
    }
  }
}
