import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {GoogleMapSearchComponent} from "./google-map-search.component";
import {MatFormFieldModule} from "@angular/material";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatIconModule} from '@angular/material/icon';

@NgModule({
  imports: [
    CommonModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
      MatIconModule
  ],
  exports:[
      GoogleMapSearchComponent
  ],
  declarations: [
      GoogleMapSearchComponent
  ]
})
export class GoogleMapSearchModule { }
