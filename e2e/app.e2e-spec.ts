import { LuanaPage } from './app.po';

describe('luana App', () => {
  let page: LuanaPage;

  beforeEach(() => {
    page = new LuanaPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
